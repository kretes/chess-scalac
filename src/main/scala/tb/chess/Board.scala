package tb.chess

case class Position(x:Int,y:Int) {
  override def toString: String = s"($x,$y)"
}

case class Board(xSize:Int, ySize:Int) {

  def getPositionsArray(x: Int, y: Int): Option[Position] = if (x>=0 && x<xSize && y>=0 && y<ySize) Some(positionsArray(x)(y)) else None

  def movePosition(diffX: Int, diffY: Int,position : Position): Option[Position] = {
    getPositionsArray(position.x+diffX,position.y+diffY)
  }

  lazy val positionsArray = Array.tabulate[Position](xSize,ySize) { (x,y) => new Position(x,y) }
  lazy val positionsSeq = positionsArray.flatten.toSeq
  lazy val positions :Set[Position] = positionsSeq.toSet
  lazy val positionsOrdered = for (y <- 0 to ySize-1;x<- 0 to xSize-1) yield getPositionsArray(x,y).get
}
