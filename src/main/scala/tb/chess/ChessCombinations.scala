package tb.chess

import scala.collection.{GenSeq, GenSet, Set}

object ChessCombinations {

  def combinationsCount(board: Board, allPieces: Seq[Piece]) = combinations(board, allPieces).size

  def printCombinations(board: Board, allPieces: Seq[Piece]) = { println(combinations(board, allPieces).mkString("\n")) }

  def combinations(board: Board, allPieces: Seq[Piece]) = {
    new CombinationsFinder(board, allPieces).exploreAllPossibleSituations
  }

  case class CombinationsFinder(board: Board, allPieces: Seq[Piece]) {

    case class PieceAtPosition(piece: Piece, position: Position) {
      lazy val positionsThreatened = piece.possibleMovements(position, board)
    }
    
    object PieceAtPosition {

      val pieces = scala.collection.mutable.Map[(Piece,Position),PieceAtPosition]()

      def get(piece: Piece, position: Position) = {
        lazy val newPieceAtPosition = new PieceAtPosition(piece,position)
        pieces.getOrElseUpdate((piece,position),newPieceAtPosition)
      }

    }
    
    val initialSituation = new Situation(Set(),Set(),Seq(),Set(),board.positions)

    def exploreAllPossibleSituations = exploreSituations(Set(initialSituation), Seq())

    case class FinalSituation(piecePositions: Set[PieceAtPosition])

    case class Situation(
              piecePositions: Set[PieceAtPosition],
              threatenedPositions: Set[Position],
              usedPieces:Seq[Piece],
              usedPositions:Set[Position],
              unthreatenedPositions:Set[Position]) {

      lazy val finalSituation = new FinalSituation(piecePositions)

      lazy val piecesToDistribute = allPieces diff usedPieces

      lazy val hasAllPiecesDistributed: Boolean = piecesToDistribute.isEmpty

      lazy val noPieceThreatened: Boolean = {
        usedPositions.find(threatenedPositions.contains).isEmpty
      }

      override def toString: String = {
        def maybeThreatened(position: Position): String = if (threatenedPositions.contains(position)) "*" else " "
        def pieceAt(position: Position): String = piecePositions.find(_.position==position).headOption.map(_.piece.toString).getOrElse(maybeThreatened(position))
        (for (position <- board.positionsOrdered) yield (if (position.x==0) "[" else "") + pieceAt(position) + (if (position.x==board.xSize-1) "]\n" else "")).mkString("")
      }

      def withPiecePosition(pieceAtPosition:PieceAtPosition):Situation = {
        val newUsedPositions = usedPositions + pieceAtPosition.position
        new Situation(
          piecePositions = piecePositions + pieceAtPosition,
          threatenedPositions = threatenedPositions ++ pieceAtPosition.positionsThreatened,
          usedPieces = usedPieces :+ pieceAtPosition.piece,
          usedPositions = newUsedPositions,
          unthreatenedPositions = unthreatenedPositions diff threatenedPositions diff newUsedPositions)
      }

      def nextSituations: Seq[Situation] = {
        for (missingPiece <- piecesToDistribute;
             position <- unthreatenedPositions;
            pieceAtPosition = PieceAtPosition.get(missingPiece,position)
            if pieceAtPosition.positionsThreatened.find(usedPositions.contains).isEmpty;
            newSituation = withPiecePosition(pieceAtPosition)) yield newSituation
      }
    }

    def getFilledAndNonConflicting(situations: GenSet[Situation]) =
      if (situations.head.hasAllPiecesDistributed) situations.toSeq.filter(_.noPieceThreatened).map(_.finalSituation).toSet else Set()

    def exploreSituations(situations: GenSet[Situation], result: GenSeq[FinalSituation]):GenSeq[FinalSituation] = situations.toSeq match {
      case Nil => result
      case _ =>
        println(s"exploring ${situations.size} ")
        val nextCandidateSituations = situations.par.flatMap(_.nextSituations).toSet
        println(s"got next candidates ${nextCandidateSituations.size} ")
        val nonConflictingSituations = getFilledAndNonConflicting(situations)
        if(nonConflictingSituations.size > 0) {
           println(s"non conflicting are ${nonConflictingSituations.size} ")
        } else {
           println(s"not all pieces distributed")
        }
        exploreSituations(nextCandidateSituations,nonConflictingSituations.toSeq ++ result)
    }

  }

}
