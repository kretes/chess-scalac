package tb.chess

import scala.collection.{GenSeq, GenSet, Set}
import scala.Predef

object ChessCombinations_Arrays {

  def combinationsCount(board: Board, allPieces: Seq[Piece]) = combinations(board, allPieces).size

  def printCombinations(board: Board, allPieces: Seq[Piece]) = { println(combinations(board, allPieces).mkString("\n")) }

  def combinations(board: Board, allPieces: Seq[Piece]) = {
    new CombinationsFinder(board, allPieces).allUniqueSituations
  }

  def arrayToString(array: Array[Array[Piece]]): String = {
    array.map("[" + _.mkString(",") + "]").mkString("\n")
  }

  object NoPiece extends Piece{
    override val isConcrete = false

    override def toString: String = " "
  }

  object Threatened extends Piece {
    override val isConcrete: Boolean = false
    override def toString: String = "*"
  }

  case class CombinationsFinder(board: Board, allPieces: Seq[Piece]) {

    def count = exploreAllPossibleSituations.size

    val notTaken: Array[Array[Piece]] = Array.tabulate[Piece](board.xSize,board.ySize) { (x,y) => NoPiece }

    def allUniqueSituations = exploreAllPossibleSituations.map(arrayToString)
    def exploreAllPossibleSituations = exploreAndExtract(new ArraySituation(notTaken,allPieces))

    case class ArraySituation(array: Array[Array[Piece]], piecesToSpawn:Seq[Piece]) {
      val hasAllPiecesDistributed: Boolean = piecesToSpawn.isEmpty

      def canSpawn(position: Position) = {
        val pieceAtPosition: Piece = array(position.x)(position.y)
        val result: Boolean = pieceAtPosition == NoPiece
        result
      }

      override def toString: String = arrayToString(array)

    }

    def copy(original: Array[Array[Piece]]): Array[Array[Piece]] = original.map(_.clone())

    def prepareNewArray(situationArray: Array[Array[Piece]], newPiece: Piece, atPosition: Position): Array[Array[Piece]] = {

      val nextArray = copy(situationArray)
      nextArray(atPosition.x).update(atPosition.y,newPiece)
      val possibleMovements = newPiece.possibleMovements(atPosition,board).iterator
      while(possibleMovements.hasNext) {
        val positionToTake: Position = possibleMovements.next()
        val pieceAtThreatenedPosition: Piece = nextArray(positionToTake.x)(positionToTake.y)
        if(pieceAtThreatenedPosition.isConcrete) {
          return null}
        nextArray(positionToTake.x).update(positionToTake.y,Threatened)
      }
      nextArray

    }

    def exploreAndExtract(situation:ArraySituation):Set[Array[Array[Piece]]] =
      exploreSituations(situation).map(_.array)

    def exploreSituations(situation: ArraySituation):Set[ArraySituation] = {

      if (situation.hasAllPiecesDistributed) return Set(situation)

      for (position <- board.positions;
        piece:Piece <- situation.piecesToSpawn.toSet.par
        if situation.canSpawn(position);
        newSituationArray = prepareNewArray(situation.array,piece,position)
        if newSituationArray != null;
        newPiecesToSpawn = situation.piecesToSpawn diff Seq(piece);
         nextSituation:ArraySituation <- exploreSituations(new ArraySituation(newSituationArray,newPiecesToSpawn))
        if nextSituation != null) yield nextSituation
     }

  }



}
