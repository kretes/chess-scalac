package tb.chess

import scala.collection.{GenSeq, GenSet, Set}

object ChessCombinations_Deep {

  def combinationsCount(board: Board, allPieces: Seq[Piece]) = combinations(board, allPieces).size

  def printCombinations(board: Board, allPieces: Seq[Piece]) = { println(combinations(board, allPieces).mkString("\n")) }

  def combinations(board: Board, allPieces: Seq[Piece]) = {
    new CombinationsFinder(board, allPieces).exploreAllPossibleSituations
  }

  case class CombinationsFinder(board: Board, allPieces: Seq[Piece]) {

    case class PieceAtPosition(piece: Piece, position: Position) {
      lazy val positionsThreatened = piece.possibleMovements(position, board)
    }

    object PieceAtPosition {

      val pieces = scala.collection.mutable.Map[(Piece,Position),PieceAtPosition]()

      def get(piece: Piece, position: Position) = {
        lazy val newPieceAtPosition = new PieceAtPosition(piece,position)
        pieces.getOrElseUpdate((piece,position),newPieceAtPosition)
      }

    }

    def exploreAllPossibleSituations:GenSet[Set[Position]] = {
      start(InitialSituation).map(_.usedPositions).toSet
    }

    trait Situation {
      val unthreatenedPositions:Set[Position]
      val usedPositions:Set[Position]
      val noPieceThreatened: Boolean
      lazy val hasAllPiecesDistributed: Boolean = allPieces.size == usedPositions.size
    }

    case object InitialSituation extends Situation {
      override val noPieceThreatened: Boolean = true
      override val usedPositions: Set[Position] = Set()
      override val unthreatenedPositions: Set[Position] = board.positions

    }

    case class ConcreteSituation(previous:Situation,pieceAtPosition:PieceAtPosition) extends Situation{

      override lazy val noPieceThreatened: Boolean = pieceAtPosition.positionsThreatened.find(usedPositions.contains).isEmpty && previous.noPieceThreatened
      override lazy val usedPositions: Set[Position] = previous.usedPositions + pieceAtPosition.position
      override lazy val unthreatenedPositions: Set[Position] = previous.unthreatenedPositions diff pieceAtPosition.positionsThreatened diff Set(pieceAtPosition.position)
    }

    def start(situation: Situation) = {
      exploreSituations(situation,allPieces)
    }

     def exploreSituations(situation: Situation,pieces:Seq[Piece]):GenSeq[Situation] = {
        if (pieces.isEmpty && situation.noPieceThreatened) {List(situation)}
        else situations(situation,pieces.head, pieces.tail)
     }

    def situations(situation: Situation,piece:Piece,pieces:Seq[Piece]):GenSeq[Situation] = {


        for(position <- situation.unthreatenedPositions.toSeq;
        nextSituation = ConcreteSituation(situation, PieceAtPosition.get(piece, position))
        if (nextSituation.noPieceThreatened);
        explored <- exploreSituations(nextSituation,pieces)) yield explored
    }

  }

}
