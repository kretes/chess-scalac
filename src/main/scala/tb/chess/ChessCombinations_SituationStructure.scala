package tb.chess

import scala.collection.{GenSeq, GenSet, Set}

object ChessCombinations_SituationStructure {

  def combinationsCount(board: Board, allPieces: Seq[Piece]) = combinations(board, allPieces).size

  def printCombinations(board: Board, allPieces: Seq[Piece]) = { println(combinations(board, allPieces).mkString("\n")) }

  def combinations(board: Board, allPieces: Seq[Piece]) = {
    new CombinationsFinder(board, allPieces).exploreAllPossibleSituations
  }

  case class CombinationsFinder(board: Board, allPieces: Seq[Piece]) {

    case class PieceAtPosition(piece: Piece, position: Position) {
      lazy val positionsThreatened = piece.possibleMovements(position, board)
    }

    object PieceAtPosition {

      val pieces = scala.collection.mutable.Map[(Piece,Position),PieceAtPosition]()

      def get(piece: Piece, position: Position) = {
        lazy val newPieceAtPosition = new PieceAtPosition(piece,position)
        pieces.getOrElseUpdate((piece,position),newPieceAtPosition)
      }

    }

    def exploreAllPossibleSituations = {
      exploreSituations(InitialSituation).map(_.usedPositions)
    }
    
    trait Situation {
      val piecesToSpawn:Seq[Piece]
      val unthreatenedPositions:Set[Position]
      val usedPositions:Set[Position]
      val noPieceThreatened: Boolean
      lazy val hasAllPiecesDistributed: Boolean = allPieces.size == usedPositions.size
    }

    case object InitialSituation extends Situation {
      override val noPieceThreatened: Boolean = true
      override val usedPositions: Set[Position] = Set()
      override val unthreatenedPositions: Set[Position] = board.positions
      override val piecesToSpawn: Seq[Piece] = allPieces
    }

    case class ConcreteSituation(previous:Situation,pieceAtPosition:PieceAtPosition) extends Situation{
      override lazy val noPieceThreatened: Boolean = previous.noPieceThreatened && pieceAtPosition.positionsThreatened.find(previous.usedPositions.contains).isEmpty
      override lazy val usedPositions: Set[Position] = previous.usedPositions + pieceAtPosition.position
      override lazy val unthreatenedPositions: Set[Position] = previous.unthreatenedPositions diff pieceAtPosition.positionsThreatened diff Set(pieceAtPosition.position)
      override lazy val piecesToSpawn: Seq[Piece] = previous.piecesToSpawn diff Seq(pieceAtPosition.piece)
    }

     def exploreSituations(situation: Situation):Set[Situation] = {
        if (situation.hasAllPiecesDistributed) {Set(situation)}
        else {
          for (
            position <- situation.unthreatenedPositions;
            piece:Piece <- situation.piecesToSpawn.toSet.par;
            nextSituation = ConcreteSituation(situation,PieceAtPosition.get(piece,position))
            if nextSituation.noPieceThreatened;
            next <- exploreSituations(nextSituation))
          yield next
        }

     }

  }

}
