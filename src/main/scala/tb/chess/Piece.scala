package tb.chess

import scala.math

abstract class Piece {
  val isConcrete: Boolean = true

  def possibleMovements(position:Position,board:Board):Set[Position] = Set()
  def possibleMovementsSeq(position:Position,board:Board):Seq[Position] = possibleMovements(position,board).toSeq
}

object Piece {

  object King extends Piece {
    override def possibleMovements(position:Position,board:Board) =
      (for (x <- -1 to 1; y <- -1 to 1; if x != 0 || y != 0) yield board.movePosition(x,y,position)).filter(_.isDefined).map(_.get).toSet

    override def toString: String = "K"

  }

  object Queen extends Piece {
    override def possibleMovements(position:Position,board:Board) =
      Bishop.possibleMovements(position,board) ++ Rook.possibleMovements(position,board)

    override def toString: String = "Q"

  }

  case object Bishop extends Piece {
    override def possibleMovements(position:Position,board:Board) =
      (for (negativeX <- Seq(-1,1); negativeY <- Seq(-1,1); value <- 1 to math.max(board.xSize,board.ySize)) yield board.movePosition(negativeX * value,negativeY * value,position)).filter(_.isDefined).map(_.get).toSet

    override def toString: String = "B"

  }

  case object Rook extends Piece {
    override  def possibleMovements(position:Position,board:Board) =
      ((for (x <- -board.xSize to board.xSize; if x != 0) yield board.movePosition(x,0,position))
        ++
        (for (y <- -board.ySize to board.ySize; if y != 0) yield board.movePosition(0,y,position))).filter(_.isDefined).map(_.get).toSet

    override def toString: String = "R"

  }

  case object Knight extends Piece {
    override def possibleMovements(position:Position,board:Board) = {
      val knightMovements: Seq[Int] = Seq(-2, -1, 1, 2)
        (for (x <- knightMovements; y <- knightMovements; if math.abs(x) != math.abs(y)) yield board.movePosition(x, y,position)).filter(_.isDefined).map(_.get).toSet
    }

    override def toString: String = "N"

  }

}