package tb.chess

import org.specs2.matcher.Matchers
import org.specs2.mutable.Specification
import tb.chess.Piece._

class ChessCombinations_SituationStructure_Spec extends Specification with Matchers {

  "single-cell board" should {
    "have one combination for one piece" in {
      ChessCombinations_SituationStructure.combinationsCount(new Board(1, 1), Seq(King)) must beEqualTo(1)
    }
  }

  "double-cell board" should {
    "have two combination for one piece" in {
      ChessCombinations_SituationStructure.combinationsCount(new Board(2, 1), Seq(King)) must beEqualTo(2)
    }
  }

  "3 by 1 board" should {
    "have one combination for two kings" in {
      ChessCombinations_SituationStructure.combinationsCount(new Board(3, 1), Seq(King, King)) must beEqualTo(1)
    }
  }

  "1 by 3 board" should {
    "have one combination for two kings" in {
      ChessCombinations_SituationStructure.combinationsCount(new Board(1, 3), Seq(King, King)) must beEqualTo(1)
    }
  }

  "1 by 2 board" should {
    "have zero combination for two kings" in {
      ChessCombinations_SituationStructure.combinationsCount(new Board(1, 2), Seq(King, King)) must beEqualTo(0)
    }
  }

  "2 by 2 board" should {
    "have zero combination for two kings" in {
      ChessCombinations_SituationStructure.combinationsCount(new Board(2, 2), Seq(King, King)) must beEqualTo(0)
    }
  }

  "1 by 3 board" should {
    "have zero combination for two rooks" in {
      ChessCombinations_SituationStructure.combinationsCount(new Board(1, 3), Seq(Rook, Rook)) must beEqualTo(0)
    }
  }

  "2 by 2 board" should {
    "have two combination for two rooks" in {
      ChessCombinations_SituationStructure.combinationsCount(new Board(2, 2), Seq(Rook, Rook)) must beEqualTo(2)
    }
  }

  "3 by 2 board" should {
    "have 13 combinations for two knights" in {
      ChessCombinations_SituationStructure.combinationsCount(new Board(3,2), Seq(Knight, Knight)) must beEqualTo(13)
    }
  }

  "4 by 4 board" should {
    "have 8 combinations for two rooks and 4 knights" in {
      ChessCombinations_SituationStructure.combinationsCount(new Board(4,4), Seq(Rook, Rook, Knight, Knight,Knight,Knight)) must beEqualTo(8)
    }
  }

  "5x5 board with 2 Kings, 2 Queens, 2 Bishops and 1 Knight." should {
    "have 104 combinations" in {
      ChessCombinations_SituationStructure.combinationsCount(new Board(5,5), Seq(King, King, Queen, Queen, Bishop, Knight)) must beEqualTo(104)
    }
  }

  "2x2 board with 2 Bishops" should {
    "have 4 combinations" in {
      ChessCombinations_SituationStructure.combinationsCount(new Board(2,2), Seq(Bishop, Bishop)) must beEqualTo(4)
    }
  }

  "3x2 board with 2 Queens" should {
    "have 2 combinations" in {
      ChessCombinations_SituationStructure.combinationsCount(new Board(3,2), Seq(Queen, Queen)) must beEqualTo(2)
    }
  }

}
