package tb.chess

import org.specs2.matcher.Matchers
import org.specs2.mutable.Specification
import tb.chess.Piece._

class Large_ChessCombinationsSpec extends Specification with Matchers {


  "7×7 board with 2 Kings, 2 Queens, 2 Bishops and 1 Knight." should {
    "have ??? combinations" in {
      ChessCombinations.combinationsCount(new Board(6,5), Seq(King, King, Queen, Queen, Bishop, Knight)) must beEqualTo(8)
    }
  }

}
