package tb.chess

import org.specs2.matcher.Matchers
import org.specs2.mutable.Specification
import tb.chess.Piece.{Knight, Bishop, Queen, King}

class Large_ChessCombinations_SituationStructure_Spec extends Specification with Matchers {

  "7x7 board with 2 Kings, 2 Queens, 2 Bishops and 1 Knight." should {
    "have ??? combinations" in {
      ChessCombinations_SituationStructure.combinationsCount(new Board(7,7), Seq(King, King, Queen, Queen, Bishop, Bishop, Knight)) must beEqualTo(8)
    }
  }

}
